package com.appium.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class WebAppTest {

    WebDriver driver;
    DesiredCapabilities capabilities = new DesiredCapabilities();

    @BeforeTest
    public  void InitConfig(){
     //   capabilities.setCapability("deviceName", "c1607e4063effaf");
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "chrome");
        //capabilities.setCapability(CapabilityType.VERSION,"10.0");
        capabilities.setCapability(CapabilityType.VERSION,"6.0.1");
        }

    @Test
    public void testApp() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"),capabilities);
        driver.get("https://www.gmail.com");
        driver.quit();
    }

}
